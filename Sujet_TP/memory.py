import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tab = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    '''
    Permet de mélanger les cartes de la liste Tab

    Parameters
    ----------
    Tab : List
        Liste de chaîne de caractère avec un doublon de lettre pour chacune.

    Returns
    -------
    Tab : List
        Liste de chaîne de caractère mélangé.

    '''
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        g = Tab[i]
        Tab[i] = Tab[j]
        Tab[j] = g
        
    return Tab

def carte_cache(Tab):   #TODO
    '''
    Crée un liste de même longueur que l'argument Tab avec des indices quelconques

    Parameters
    ----------
    Tab : List
        Liste des cartes encore face cachée

    Returns
    -------
    L_new : List
        Liste de même taille que Tab rempli par des indices quelconques.

    '''
    i=len(Tab)
    L_new=[]
    for j in range (i):
        L_new.append(j+1)
    return L_new
    

def choisir_cartes(Tab):
    c1 = int(input("Choisissez une carte : "))
    print(Tab[c1])
    c2 = int(input("Choisissez une deuxieme carte : "))
    while c1 == c2:
        print("Erreur, la deuxieme carte ne peut être la même que la premiere ! ")
        c2 = int(input("Veuillez choisir une deuxieme carte différente de la premiere : "))
    print(Tab[c2])
    return [Tab[c1],Tab[c2]]


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
        c1/c2 : int nombre correpondant au numéro dans la liste 1ère carte = 1
        Tab : string list correpond au cartes mélangés
        Tab_cache : sortie string list/ même format que tab mais toutes les cartes hors c1/c2 sont cachee
        TODO
        doit retourner les cartes dans la liste cachée
    """

    if Tab[c1] == Tab[c2] :
        Tab_cache[c1],Tab_cache[c2] = Tab[c1],Tab[c2]
        return Tab_cache
    else :
        Tab_cache_2 = Tab_cache
        Tab_cache_2[c1],Tab_cache_2[c2] = Tab[c1],Tab[c2]
        return Tab_cache

    


def jouer(Tab):
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo tu as gagné!!!")


    






if __name__ == "__main__" :
    
    #Mélange
    print('-----Mélange------')
    print('Expected')
    print(Tab)
    print('\nGet : la liste précédente mélangée')
    print (melange_carte(Tab))
    
    #Choix des 2 cartes
    print('-----Choix des 2 cartes-----')
    print (choisir_cartes(Tab))
    
    #Retourne cartes
    print('-----Retourne les cartes 4 et 6-----')
    c1 = 4
    c2 = 6
    Tab_cache = ['', '', '', '', '', '', '', '', '', '', '', '']
    
    
    print (retourne_carte (c1, c2, Tab, Tab_cache))
    
    
    
    
    
    
    
    
    
    

